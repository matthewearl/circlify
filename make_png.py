#!/usr/bin/env python

# Copyright (c) 2016 Matthew Earl
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
#     The above copyright notice and this permission notice shall be included
#     in all copies or substantial portions of the Software.
# 
#     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#     OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
#     NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
#     DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
#     OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
#     USE OR OTHER DEALINGS IN THE SOFTWARE.


import json
import sys

import cv2
import numpy as np


PRE_SCALE = 10
OUT_SCALE = 2

def scale_shape(t, f):
    return tuple(f * x for x in t[:2]) + tuple(t[2:])

with open(sys.argv[1]) as f:
    d = json.load(f)

info = d['info']

pre_shape = scale_shape(d['shape'], PRE_SCALE)
im = np.zeros(pre_shape, dtype=np.int32)

if info['type'] == 'circles':
    for center, r, color in info['nodes']:
        center = scale_shape(center, PRE_SCALE)
        r *= PRE_SCALE

        b = np.zeros(pre_shape[:2], dtype=np.uint8)
        cv2.circle(b, tuple(center), r, 1, -1)
        im += b[..., np.newaxis] * np.array(color)

    im = np.clip(im, 0, 255).astype(np.uint8)
    im = cv2.resize(im, scale_shape(d['shape'][:2], OUT_SCALE))
    cv2.imwrite(sys.argv[2], im)
else:

    raise Exception("Unsupported type {!r}".format(info['type']))

