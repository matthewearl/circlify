#!/usr/bin/env python

# Copyright (c) 2016 Matthew Earl
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
#     The above copyright notice and this permission notice shall be included
#     in all copies or substantial portions of the Software.
# 
#     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#     OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
#     NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
#     DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
#     OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
#     USE OR OTHER DEALINGS IN THE SOFTWARE.

import json
import sys


with open(sys.argv[1]) as f:
    d = json.load(f)


print '<svg height="{}" width="{}">'.format(*d['shape'])

info = d['info']
if info['type'] == 'lines':
    for p1, p2 in zip(info['nodes'], info['nodes'][1:]):
        print ('<line x1="{}" y1="{}" x2="{}" y2="{}"'
               ' style="stroke:rgb({c},{c},{c});stroke-width:{}" />').format(
                           p1[0], p1[1], p2[0], p2[1], info['line_width'], c=0)
else:

    raise Exception("Unsupported type {!r}".format(info['type']))

print '</svg>'

