#!/usr/bin/env python

# Copyright (c) 2016 Matthew Earl
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy # of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
#     The above copyright notice and this permission notice shall be included
#     in all copies or substantial portions of the Software.
# 
#     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#     OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
#     NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
#     DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
#     OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
#     USE OR OTHER DEALINGS IN THE SOFTWARE.


__all__ = (
    'circlify',
)


import argparse
import json
import logging
import math
import random
import sys

import cv2
import numpy as np


logger = logging.getLogger("circlify")


class CircleState(object):
    _NEW_NODE_PROB = 0.2
    _DEL_NODE_PROB = 0.2
    _NEAR_PROB = 0.4
    _REPLACE_PROB = 0.2
    _NEAR_RANGE = 2
    _MAX_NODES = 50
    _MAX_COLOR = 64

    def __init__(self, im, nodes, accum=None):
        self._im = im
        self._nodes = nodes

        if len(self._im.shape) == 2:
            self._im = self._im[..., np.newaxis]

        if accum is None:
            assert len(nodes) == 0
            accum = np.zeros(self._im.shape, dtype=np.int64)
        self._accum = accum

    def _node_to_array(self, n):
        center, radius, color = n

        b = np.zeros(self._im.shape[:2], dtype=np.uint8)
        cv2.circle(b, center, radius, 128, -1)
        #blur = radius | 1
        #b = cv2.GaussianBlur(b, (blur, blur), 0)

        return (b.astype(np.int32)[..., np.newaxis] * color) / 128

    @property
    def accum(self):
        accum = np.clip(self._accum, 0, 255).astype(np.uint8)
        #accum = 255 - accum

        return accum

    @property
    def energy(self):
        scale_factor = 1.0 / (self._im.shape[0] * self._im.shape[1] * 2 ** 16)
        return np.sum((self.accum.astype(np.float32) -
                       self._im.astype(np.float32)) ** 2) * scale_factor

    def _rand_color(self, min_color, max_color):
        return np.array([random.randint(min_color, max_color)
                                            for i in range(self._im.shape[2])],
                        dtype=np.int32)

    def _rand_node(self):
        return ((random.randint(0, self._im.shape[1] - 1),
                 random.randint(0, self._im.shape[0] - 1)),
                 random.randint(0, min(*self._im.shape[:2])),
                 self._rand_color(-self._MAX_COLOR, self._MAX_COLOR))

    def _near_point(self, p):
        (x, y), radius, color = p
        color = color.copy()
        
        x += random.randint(-self._NEAR_RANGE, self._NEAR_RANGE)
        x = max(0, min(self._im.shape[1] - 1, x))

        y += random.randint(-self._NEAR_RANGE, self._NEAR_RANGE)
        y = max(0, min(self._im.shape[0] - 1, y))

        radius += random.randint(-self._NEAR_RANGE, self._NEAR_RANGE)
        radius = max(1, min(min(*self._im.shape[:2]), radius))

        color += self._rand_color(-self._NEAR_RANGE, self._NEAR_RANGE)
        color = np.clip(color, -self._MAX_COLOR, self._MAX_COLOR)

        return (x, y), radius, color

    def get_neighbour(self):
        r = random.random()
        nodes = self._nodes[:]
        accum = self._accum.copy()

        def new(accum):
            nodes.insert(len(nodes), self._rand_node())
            accum += self._node_to_array(nodes[-1])
            return accum

        def delete(accum):
            idx = random.randint(0, len(nodes) - 1)
            accum -= self._node_to_array(nodes[idx])
            del nodes[idx]
            return accum

        def near(accum):
            idx = random.randint(0, len(nodes) - 1)
            accum -= self._node_to_array(nodes[idx])
            nodes[idx] = self._near_point(nodes[idx])
            accum += self._node_to_array(nodes[idx])
            return accum

        def replace(accum):
            idx = random.randint(0, len(nodes) - 1)
            accum -= self._node_to_array(nodes[idx])
            nodes[idx] = self._rand_node()
            accum += self._node_to_array(nodes[idx])
            return accum

        outcomes = []
        if len(nodes) < self._MAX_NODES:
            outcomes.append((self._NEW_NODE_PROB, new))
        if len(nodes) > 0:
            outcomes.append((self._DEL_NODE_PROB, delete))
            outcomes.append((self._REPLACE_PROB, replace))
            outcomes.append((self._NEAR_PROB, near))

        r *= sum(p for p, f in outcomes)
        t = 0.
        for p, f in outcomes:
            t += p
            if r <= t:
                accum = f(accum)
                break
        else:
            assert False

        return CircleState(self._im, nodes, accum)

    def info(self):
        return {'type': 'circles',
                'nodes': [(p, r, tuple(int(x) for x in c))
                                                   for p, r, c in self._nodes]}


class LineState(object):
    _NEW_NODE_PROB = 0.2
    _DEL_NODE_PROB = 0.2
    _NEAR_PROB = 0.4
    _MAX_NODES = 225
    _LINE_WEIGHT = 80
    _LINE_WIDTH = 2
    _NEAR_RANGE = 3

    def __init__(self, im, nodes):
        self._im = im
        self._nodes = nodes

    def _rand_node(self):
        r = random.randint(0, 2 * (self._im.shape[0] + self._im.shape[1]) - 1)

        if r < self._im.shape[0]:
            return (0, r)
        r -= self._im.shape[0]
        if r < self._im.shape[0]:
            return (self._im.shape[1] - 1, r)
        r -= self._im.shape[0]
        if r < self._im.shape[1]:
            return (r, 0)
        r -= self._im.shape[1]
        assert r < self._im.shape[1]

        return (r, self._im.shape[0] - 1)

    def _rand_node2(self):
        return (random.randint(0, self._im.shape[1] - 1),
                random.randint(0, self._im.shape[0] - 1))

    def _near_point(self, p):
        
        p = tuple(x + random.randint(-self._NEAR_RANGE, self._NEAR_RANGE)
                                                                    for x in p)
        p = tuple(max(0, x) for x in p)
        p = (min(self._im.shape[1], p[0]), min(self._im.shape[0], p[1]))
        return p

    @property
    def accum(self):
        accum = np.ones(self._im.shape, dtype=np.uint8) * 255
        for p1, p2 in zip(self._nodes, self._nodes[1:]):
            #b = np.zeros(self._im.shape, dtype=np.int8)
            #cv2.line(b, p1, p2, self._LINE_WEIGHT, self._LINE_WIDTH)
            #accum = np.maximum(0, accum.astype(np.int32) - b.astype(np.int32))
            cv2.line(accum, p1, p2, 0, self._LINE_WIDTH)

        accum = cv2.GaussianBlur(accum, (31, 31), 0)

        return accum

    @property
    def energy(self):
        scale_factor = 1.0 / (self._im.shape[0] * self._im.shape[1] * 2 ** 16)
        return np.sum((self.accum.astype(np.float32) -
                       self._im.astype(np.float32)) ** 2) * scale_factor

    def get_neighbour(self):
        r = random.random()
        nodes = self._nodes[:]

        if ((r < self._NEW_NODE_PROB and len(nodes) < self._MAX_NODES)
             or len(nodes) == 0):
            nodes.insert(random.randint(0, len(nodes)), self._rand_node2())
        elif (self._NEW_NODE_PROB < r <= self._DEL_NODE_PROB
              and len(self._nodes) != 0):
            del nodes[random.randint(0, len(nodes) - 1)]
        elif (self._NEW_NODE_PROB + self._DEL_NODE_PROB < r <=
                                                              self._NEAR_PROB):
            idx = random.randint(0, len(nodes) - 1)
            nodes[idx] = self._near_point(nodes[idx])
        else:
            nodes[random.randint(0, len(nodes) - 1)] = self._rand_node2()

        return LineState(self._im, nodes)

    def info(self):
        return {'type': 'lines',
                'nodes': self._nodes,
                'line_weight': self._LINE_WEIGHT,
                'line_width': self._LINE_WIDTH}
        

def im_writer():
    while True:
        for i in range(10):
            im = yield


def circlify(im, initial_state, initial_temperature, iterations_per_phase,
             energy_range_limit):
    state = initial_state
    T = initial_temperature
    energy = state.energy
    min_energy = max_energy = energy
    total_energy = energy

    cv2.namedWindow("accum")
    it = 0
    try:
        while True:
            next_state = state.get_neighbour()
            next_energy = next_state.energy

            if next_energy < energy:
                accept_prob = 1.0
            else:
                accept_prob = math.exp(-(next_energy - energy) / T)

            if random.random() < accept_prob:
                state = next_state
                energy = next_energy

                logger.debug("%s: T: %s, Energy: %s, #nodes: %s",
                             it, T, energy, len(state._nodes))

                cv2.imshow("accum", state.accum)
                cv2.waitKey(1)

            it += 1

            if it % iterations_per_phase == 0:
                T *= 0.99

            if it % 10000 == 0:
                energy_range = max_energy - min_energy
                logger.info("%s: T: %s, min: %s, max: %s, avg: %s, #: %s",
                            it, T, min_energy, max_energy,
                            total_energy / 10000.,
                            len(state._nodes))
                if energy_range < energy_range_limit:
                    logger.info("Energy range %s is less than limit %s.",
                                energy_range, energy_range_limit)
                    break
                total_energy = min_energy = max_energy = energy
            else:
                min_energy = min(energy, min_energy)
                max_energy = max(energy, max_energy)
                total_energy += energy


        cv2.destroyWindow("accum")

    except KeyboardInterrupt:
        logger.info("Caught keyboard interrupt.")

    return state.accum, {'shape': im.shape, 'info': state.info()}

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Circlify or linify an image')
    parser.add_argument('--initial-temperature', type=float,
                        help='Initial temperature',
                        default=0.001)
    parser.add_argument('--iterations-per-phase', type=int,
                        help='Number of iterations for each phase. After each '
                             'phase the temperature is reduced by a factor of '
                             '0.99.',
                        default=500)
    parser.add_argument('--energy-range-limit', type=float,
                        help='Execution is terminated if the difference '
                             'between the minimum and maximum energies seen '
                             'in a 10000 iteration period is less than this '
                             'value',
                        default=0.00001)
    parser.add_argument('--mode', type=str,
                        help='Either \'circles\' or \'lines\'.',
                        required=True)
    parser.add_argument('input_image', type=str,
                        help='Image to render as circles / lines')
    parser.add_argument('--output-image', type=str,
                        help='Filename to which output image will be written',
                        required=True)
    parser.add_argument('--output-json', type=str,
                        help='Filename to which output JSON will be written',
                        required=True)
    parser.add_argument('--debug', help='Print debug information',
                        action='store_true')
    args = parser.parse_args()


    logging.getLogger().addHandler(logging.StreamHandler())
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)

    im = cv2.imread(args.input_image)
    if im is None:
        raise Exception('Failed to read input image {!r}'.format(
                                                             args.input_image))

    if args.mode == 'circles':
        initial_state = CircleState(im, [])
    elif args.mode == 'lines':
        im = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
        initial_state = LineState(im, [])
    else:
        raise Exception('Invalid mode {}'.format(args.mode))

    out_im, out_json = circlify(im,
                                initial_state,
                                args.initial_temperature,
                                args.iterations_per_phase,
                                args.energy_range_limit)

    cv2.imwrite(args.output_image, out_im)
    with open(args.output_json, "w") as f:
        json.dump(out_json, f)

